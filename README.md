This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Add an .env file in the project directory with a key named REACT_APP_PIXABAY_API_KEY
## How to start

In the project directory, you can run:
### `npm i`

then
### `npm start`

which runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />

### `npm test`

Launches the test runner in the interactive watch mode.<br />
