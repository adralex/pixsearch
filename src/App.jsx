import {
  ChakraProvider,
  Grid,
  theme,
} from '@chakra-ui/react';
import ColorModeSwitcher from './components/ColorModeSwitcher';
import SearchPage from './components/SearchPage';

function App() {
  return (
    <ChakraProvider theme={theme}>
      <Grid p={10}>
        <ColorModeSwitcher />
        <SearchPage />
      </Grid>
    </ChakraProvider>
  );
}

export default App;
