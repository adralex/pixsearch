/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-undef */
import { screen } from '@testing-library/react';
import { render } from '../test-utils';
import SearchBar from '../components/SearchBar';

const props = {
  value: 'test',
  handleChange: () => { },
  handleClick: () => { },
};

test('renders search field', () => {
  render(<SearchBar {...props} />);
  const input = screen.getByPlaceholderText('Enter search phrase');
  expect(input).toBeInTheDocument();
});

test('renders search button', () => {
  render(<SearchBar {...props} />);
  const linkElement = screen.getByText('Go');
  expect(linkElement).toBeInTheDocument();
});
