/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-undef */
import { screen } from '@testing-library/react';
import { render } from '../test-utils';
import ColorModeSwitcher from '../components/ColorModeSwitcher';

test('renders color mode switcher', () => {
  render(<ColorModeSwitcher />);
  const linkElement = screen.getByLabelText(/Switch to/i);
  expect(linkElement).toBeInTheDocument();
});
