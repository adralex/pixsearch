import PropTypes from 'prop-types';
import {
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@chakra-ui/icons';
import {
  Button,
  ButtonGroup,
  IconButton,
  Text,
} from '@chakra-ui/react';
import usePagination from '../hooks/usePagination';

function Pagination(props) {
  const {
    page,
    perPage,
    allItemsCount,
    onPageChange,
  } = props;

  const paginationRange = usePagination(page, perPage, allItemsCount);

  if (page === 0 || paginationRange.length < 2) {
    return null;
  }

  const handlePrevious = () => {
    onPageChange(page - 1);
  };

  const handleNext = () => {
    onPageChange(page + 1);
  };

  const lastPage = paginationRange[paginationRange.length - 1];

  return (
    <ButtonGroup>
      <IconButton
        aria-label="previous"
        onClick={handlePrevious}
        icon={<ChevronLeftIcon />}
        isDisabled={page === 1}
      />
      {paginationRange.map((pageNumber, i) => {
        if (pageNumber === 'GAP') {
          // eslint-disable-next-line react/no-array-index-key
          return <Text key={`gap${i}`}>...</Text>;
        }

        return (
          <Button
            key={pageNumber}
            onClick={() => onPageChange(pageNumber)}
            isDisabled={pageNumber === page}
            variant="ghost"
          >
            {pageNumber}
          </Button>
        );
      })}
      <IconButton
        aria-label="next"
        onClick={handleNext}
        icon={<ChevronRightIcon />}
        isDisabled={page === lastPage}
      />
    </ButtonGroup>
  );
}

Pagination.defaultProps = {
  perPage: 20,
};

Pagination.propTypes = {
  page: PropTypes.number.isRequired,
  perPage: PropTypes.number,
  allItemsCount: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
