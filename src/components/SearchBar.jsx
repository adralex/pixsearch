import PropTypes from 'prop-types';
import {
  Button,
  Input,
  Grid,
} from '@chakra-ui/react';
import { SearchIcon } from '@chakra-ui/icons';

function SearchBar(props) {
  const {
    value,
    handleChange,
    handleClick,
    isLoading,
  } = props;

  return (
    <Grid
      templateColumns="repeat(auto-fill, minmax(10rem, 20rem))"
      justifyContent="center"
      gap={10}
    >
      <Input
        placeholder="Enter search phrase"
        focusBorderColor="teal.500"
        autoFocus
        value={value}
        onChange={handleChange}
        onKeyDown={(e) => e.key === 'Enter' && handleClick()}
      />
      <Button
        width="50%"
        type="submit"
        onClick={handleClick}
        leftIcon={<SearchIcon />}
        colorScheme="teal"
        isLoading={isLoading}
        loadingText="Loading"
      >
        Go
      </Button>
    </Grid>
  );
}

SearchBar.defaultProps = {
  isLoading: false,
};

SearchBar.propTypes = {
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};

export default SearchBar;
