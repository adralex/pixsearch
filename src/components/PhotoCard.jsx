import PropTypes from 'prop-types';
import {
  Avatar,
  Box,
  HStack,
  Image,
  Tag,
  Text,
  VStack,
  SlideFade,
} from '@chakra-ui/react';
import useLoadImage from '../hooks/useLoadImage';

function PhotoCard({ item, searchByTag }) {
  const {
    webformatURL,
    user,
    userImageURL,
    tags,
  } = item;

  const { isLoaded, handleLoadImage, loadStyle } = useLoadImage();

  return (
    <Box
      maxWidth="sm"
      borderRadius="lg"
    >
      <Image
        src={webformatURL}
        alt="photo"
        onLoad={handleLoadImage}
        style={{ ...loadStyle.image }}
      />
      <SlideFade
        offsetY="30px"
        in={isLoaded}
      >
        <Box p="2">
          <VStack align="flex-start" justifyContent="space-between">
            <HStack>
              <Avatar size="sm" bg="teal.200" name={user} src={userImageURL} />
              <Text fontSize="md">{user}</Text>
            </HStack>
            <HStack>
              {tags?.split(', ')
                .map((t) => (
                  <Tag
                    as="button"
                    key={t}
                    variant="solid"
                    colorScheme="teal"
                    onClick={() => searchByTag(t)}
                  >
                    {`#${t}`}
                  </Tag>
                ))}
            </HStack>
          </VStack>
        </Box>
      </SlideFade>
    </Box>
  );
}

PhotoCard.defaultProps = {
  searchByTag: () => {},
};

PhotoCard.propTypes = {
  item: PropTypes.shape({
    webformatURL: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    userImageURL: PropTypes.string.isRequired,
    tags: PropTypes.string.isRequired,
  }).isRequired,
  searchByTag: PropTypes.func,
};

export default PhotoCard;
