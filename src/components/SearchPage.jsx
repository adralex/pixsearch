import { useState } from 'react';
import {
  Alert,
  AlertDescription,
  AlertIcon,
  AlertTitle,
  Grid,
  GridItem,
  Text,
} from '@chakra-ui/react';

import Pagination from './Pagination';
import PhotoCard from './PhotoCard';
import SearchBar from './SearchBar';
import useFetch from '../hooks/useFetch';

function SearchPage() {
  const baseURI = `https://pixabay.com/api/?key=${process.env.REACT_APP_PIXABAY_API_KEY}`;

  const [inputField, setInputField] = useState('');
  const [searchTerm, setSearchTerm] = useState(inputField);
  const [currentPage, setCurrentPage] = useState(1);

  const [data, isLoaded, error] = useFetch(baseURI, { searchTerm, currentPage });

  const handleNewSearch = () => { setSearchTerm(inputField); setCurrentPage(1); };
  const handleInputChange = (event) => setInputField(event.target.value);
  const handleSearchByTag = (tag) => setInputField(tag);
  const handlePageChange = (page) => { setCurrentPage(page); };

  if (error) {
    return (
      <Grid gap={10}>
        <GridItem>
          <SearchBar
            value={inputField}
            handleChange={handleInputChange}
            handleClick={handleNewSearch}
          />
        </GridItem>
        <GridItem>
          <Alert status="error">
            <AlertIcon />
            <AlertTitle>Oops!</AlertTitle>
            <AlertDescription>
              There was an error processing your request.
            </AlertDescription>
          </Alert>
        </GridItem>
      </Grid>
    );
  }

  return (
    <Grid gap={10}>
      <GridItem>
        <SearchBar
          value={inputField}
          handleChange={handleInputChange}
          handleClick={handleNewSearch}
          isLoading={!isLoaded}
        />
      </GridItem>
      <Grid
        templateColumns="repeat(auto-fit, minmax(10rem, 20rem))"
        justifyContent="center"
        gap={10}
      >
        {
          data?.hits.map((item) => (
            <GridItem key={item.id}>
              <PhotoCard item={{ ...item }} searchByTag={handleSearchByTag} />
            </GridItem>
          ))
        }
      </Grid>
      {
        (isLoaded && data?.hits.length === 0) && (
          <GridItem>
            <Text fontSize="lg">No results found for this search</Text>
          </GridItem>
        )
      }
      <GridItem justifySelf="end" alignSelf="end">
        <Pagination
          page={currentPage}
          allItemsCount={data?.totalHits || 1}
          onPageChange={handlePageChange}
        />
      </GridItem>
    </Grid>
  );
}

export default SearchPage;
