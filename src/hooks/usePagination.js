import { useMemo } from 'react';
import range from '../utils/range';

const PAGES_ADJACENT = 1;
const ALL_PAGE_BUTTONS = 6;
const GAP = 'GAP';

const usePagination = (page, perPage, allItemsCount) => {
  const paginationRange = useMemo(() => {
    const lastPage = Math.ceil(allItemsCount / perPage);
    const totalPageNumbers = PAGES_ADJACENT + ALL_PAGE_BUTTONS;
    const leftAdjacent = Math.max(1, page - PAGES_ADJACENT);
    const rightAdjacent = Math.min(page + PAGES_ADJACENT, lastPage);
    const leftGap = leftAdjacent > 2;
    const rightGap = rightAdjacent < (lastPage - 2);

    if (totalPageNumbers >= lastPage) {
      return range(1, lastPage);
    }

    if (!leftGap && rightGap) {
      const leftRange = range(1, ALL_PAGE_BUTTONS);
      return [...leftRange, GAP, lastPage];
    }

    if (leftGap && rightGap) {
      const middleRange = range(leftAdjacent, rightAdjacent + 1);
      return [1, GAP, ...middleRange, GAP, lastPage];
    }

    if (leftGap && !rightGap) {
      const endRange = range(lastPage - ALL_PAGE_BUTTONS + 2, lastPage + 1);
      return [1, GAP, ...endRange];
    }
    return [1];
  }, [page, perPage, allItemsCount]);

  return paginationRange;
};

export default usePagination;
