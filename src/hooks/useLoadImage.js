import { useState } from 'react';

function useLoadImage() {
  const [isLoaded, setIsLoaded] = useState(false);

  const handleLoadImage = () => {
    setIsLoaded(true);
  };

  const loadStyle = {
    image: {
      opacity: isLoaded ? 1 : 0,
      transition: 'opacity 700ms ease-in 10ms',
    },
  };

  return { isLoaded, handleLoadImage, loadStyle };
}

export default useLoadImage;
