import {
  useState,
  useEffect,
} from 'react';

const useFetch = (base, params) => {
  const { searchTerm, currentPage } = params;
  const [data, setData] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    setIsLoaded(false);
    fetch(`${base}&q=${searchTerm}&page=${currentPage}`)
      .then((res) => {
        if (res.status < 200 && res.status > 299) {
          setIsLoaded(true);
          setError(res.statusText);
        }
        setIsLoaded(true);
        return res.json();
      })
      .then((res) => setData(res))
      .catch((err) => {
        setIsLoaded(true);
        setError(err);
      });
  }, [base, searchTerm, currentPage]);
  return [data, isLoaded, error];
};

export default useFetch;
